/*
//Clase lista simple con constructor, destructor, y funciones miembro para poner y obtener el puntero al  primer nodo de la lista*/


public class NODOEJ{
	public static void main(String[] args) {
	Lista l1;
	Nodo inicio, n2, n3, n4;
	Elemento e1, e2, e3, e4;
	e1=new Elemento(40);
	e2=new Elemento(25);
	e3=new Elemento(10);
	e4=new Elemento(4);
	inicio=new Nodo(e1);
	l1=new Lista(inicio);
	n2=new Nodo(e2);
	l1.insertarInicio(n2);
	n3=new Nodo(e3);
	l1.insertarInicio(n3);
	n4=new Nodo(e4);
	l1.insertarInicio(n4);
    System.out.println(" Lista: "+l1);
	}
}



class Elemento{
        int valor;
        public int getValor(){
        return valor;
        }

        public void setValor(int valor){ //modificadores
        this.valor=valor;
        }

        @Override
        public String toString(){
        String nuevaPinta = "Valor =(" + valor+ ")";
        return nuevaPinta;
        }

        public Elemento(){
        valor=0;
        }

        public Elemento(int valor){
        this.valor=valor;
        }

        }

    //Declaración e implementación de la clase Nodo
class Nodo{

        private Elemento e;
        private Nodo sig;

        public Nodo(){
        e=null;
        sig=null;
        }

        public Nodo(Elemento e){
        this.e=e;
        sig=null;
        }

        public Nodo(Elemento e, Nodo sig){
        this.e=e;
        this.sig=sig;
        }

        public Elemento getE(){ //accesores
        return e;
        }

        public Nodo getSig(){
        return sig;
        }

        public void setE(Elemento e){ //modificadores
        this.e=e;
        }

        public void setSig(Nodo sig){
        this.sig=sig;
        }

        @Override
        public String toString(){
        String nuevaPinta = "[ "+ e +" ]";
        return nuevaPinta;
        }

}

class Lista{
        protected Nodo inicio;

        public Lista(){
        inicio=null;
        }

        public Lista(Nodo inicio){
        this.inicio=inicio;
        }

        public Nodo getInicio(){ //accesores
        return inicio;
        }

        public void setInicio(Nodo inicio){
        this.inicio=inicio;
        }

        @Override
        public String toString(){

        String nuevaPinta = "{ ";
        for(Nodo i=inicio; i!=null; i=i.getSig()){
        nuevaPinta+= i;
        if(i.getSig()!=null ){
        nuevaPinta+=",";
        }

        }
        nuevaPinta += " }";
        return nuevaPinta;
        }

        public void insertarInicio(Nodo inicio){
        Nodo insertar = this.copiarNodo(inicio);
        insertar.setSig(this.inicio);
        this.inicio=insertar;
        }

        private Nodo copiarNodo(Nodo original){
        Nodo copia;
        Elemento enuevo;
        enuevo = original.getE();
        copia = new Nodo(enuevo);
        return copia;
        }






}
