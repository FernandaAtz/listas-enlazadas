/*
Declaración e implementación de la clase Nodo que contiene una información de tipo elemento y el siguiente de tipo puntero a Nodo.
La clase Nodo tiene dos atributos protegidos que son el elemento e, y Sig que es un puntero a la propia clase Nodo. Ambos atributos sirven para almacenar la información del nodo, y la dirección del siguiente nodo. Se declaran como funciones miembro tres constructores. Estos constructores son: el constructor por defecto; el constructor que inicializa el atributo x del Nodo al valor de x, y pone el atributo Sig a NULL; el constructor, que inicializa los dos atributos del Nodo. Las funciones miembro encargadas de Obtener y Poner tanto el elemento e como el puntero Sig son: Telemento OE(); void PE(Telemento e); Nodo * OSig(); y void PSig(Nodo *p);. Por último, la función miembro
destructor, se declara por defecto. El tipo elemento de la clase Nodo es un entero, pero al estar definido en un typedef, puede cambiarse de una forma sencilla y rápida para que almacene cualquier otro tipo de información.*/


public class NODO{
	public static void main(String[] args) {
	}
}

class Nodo{
        protected double real;
        protected double imaginario;

        public Complejo(){
        real=0.0;
        imaginario=0.0;
        }

        public Complejo(double real, double imaginario){
        this.real=real;
        this.imaginario=imaginario;
        }

        public double getReal(){ //accesores
        return real;
        }

        public double getImaginario(){
        return imaginario;
        }

        public void setReal(double real){ //modificadores
        this.real=real;
        }

        public void setImaginario(double imaginario){
        this.imaginario=imaginario;
        }

        @Override
        public String toString(){
        String nuevaPinta = "(" + real + " +" + imaginario + "i)";
        return nuevaPinta;
        }

}

